//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by pcl_extension_template.rc
//
#define IDS_PROJNAME                    100
#define IDR_PCL_EXTENSION_TEMPLATE      101
#define IDR_PCLLIBRARY                  102
#define IDR_PCLTYPE                     103
#define IDR_PCLOBJECT                   104
#define IDR_TYPE1TYPE                   105
#define IDR_TYPE1                       106

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           109
#endif
#endif
