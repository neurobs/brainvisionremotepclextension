This project builds a PCL extension (plug-in) for the Presentation stimulus delivery software from Neurobehaviora Systems (www.neurobs.com). The extension manages communication with the BrainVision Remote Control Server 2.0 and Brain Vision Recorder from Brain Products (http://www.brainproducts.com/). To use this extension, the Brain Vision Recorder must be installed, and the BrainVision Remote Control Server 2.0 must be running.

The project has the following dependencies:
- Visual Studio 2015
- Presentation SDK (available from www.neurobs.com)
- Boost 1.64.0 C++ library

The project is expecting the following relative file locations:
this_repository/*
PresentationSDK/*
boost/boost_1_64_0/*

See the Presentation documentation for usage details.

